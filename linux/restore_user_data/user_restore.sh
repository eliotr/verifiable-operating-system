#!/bin/bash

#    Simple proof of concept script to restore user files 
#    Copyright (C) 2019 Eliot Roxbergh <e@r0x.se> 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


declare -a users=("user")
declare -a adminDirs=("/boot" "/default_conf" "/initrd.img" "/initrd.img.old"
  "/opt" "/root"  "/srv" "/vmlinuz" "/vmlinuz.old")

#Note that we change the owner to root!
echo "Blocking read, write and execute to sensitive directories"
for dir in "${adminDirs[@]}"
do
  chown -R root:root "$dir" 
	chmod -R o-rwx "$dir"
done

for user in "${users[@]}"
do 
	echo "Restoring user files for $user"
	cp /default_conf/home/. "/home/$user" -rfa
	chown -R "$user":"$user" "/home/$user"
done
