#!/bin/bash -e

#    Lazy script to create and enable the systemd start up script
#    Copyright (C) 2019 Eliot Roxbergh <e@r0x.se> 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


#su
SCRIPT_PATH="/usr/local/bin/user_restore.sh"
cp user_restore.sh "$SCRIPT_PATH"
chown root:root "$SCRIPT_PATH"
chmod 770 "$SCRIPT_PATH"

UNIT_PATH="/etc/systemd/system/user_restore.service"
cp user_restore.service "$UNIT_PATH"
chown root:root "$UNIT_PATH"
chmod 660 "$UNIT_PATH" 

systemctl daemon-reload
systemctl enable user_restore.service

