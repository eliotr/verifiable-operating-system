#!/bin/bash -e

#    show files with both write and execute for the current user
#    Copyright (C) 2019 Eliot Roxbergh <e@r0x.se> 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#    regular users shouldn't have write AND execute in persistent folders outside /home
#    regular users shouldn't be able to create such files

DIR="/"
LOGDIR="results.txt"
date > "$LOGDIR"

echo; echo "Checking for files with o+wx"
REQ1="find "$DIR" -perm /001  -type f"
REQ2="find "$DIR" -perm /002  -type f"
RES=$(comm -12 <($REQ1 2>/dev/null | sort) <($REQ2 2>/dev/null | sort))
if [ -n "${RES}" ]; then
  #! Recommended to manually change permissions to avoid breaking
  #echo "Fixing permissions of:"
  #echo "$RES" | while read line; do chmod o-wx "$line"; done
  echo "$RES" >> "$LOGDIR"
  echo "$RES"
else
  echo "No matches"
fi

echo; echo "Checking for files with g+wx, belonging to user group $GROUPS"
REQ1="find "$DIR" -perm /010  -type f -group $GROUPS" #TODO Doesn't show all groups the user belong to?
REQ2="find "$DIR" -perm /020  -type f -group $GROUPS"
RES=$(comm -12 <($REQ1 2>/dev/null | sort) <($REQ2 2>/dev/null | sort))
if [ -n "${RES}" ]; then
  #! Recommended to manually change permissions to avoid breaking
  #echo "Fixing permissions of:"
  #echo "$RES" | while read line; do chmod g-wx "$line"; done
  echo "$RES" >> "$LOGDIR"
  echo "$RES"
else
  echo "No matches"
fi

echo; echo "Checking for files with u+wx, belonging to $USER"
REQ1="find "$DIR" -perm /100  -type f -user $USER"
REQ2="find "$DIR" -perm /200  -type f -user $USER"
RES=$(comm -12 <($REQ1 2>/dev/null | sort) <($REQ2 2>/dev/null | sort))
if [ -n "${RES}" ]; then
  #! Recommended to manually change permissions to avoid breaking
  #echo "Fixing permissions of:"
  #echo "$RES" | while read line; do chmod u-wx "$line"; done
  echo "$RES" >> "$LOGDIR"
  echo "$RES"
else
  echo "No matches"
fi
