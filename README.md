# Verifiable Operating System

**The report has been revised, however, the Discussion is not yet finished.**

Verifiable Operating System was a side project by me Eliot Roxbergh and my colleague Emil Hemdal.
In the report we argue about the need for user configurable boot software (here Coreboot and GRUB which boots Linux), and we show a number of steps which could be performed to harden this system.
We then go on to discuss some ideas of how this setup could be configured to fit different use cases aimed at security.