#    Rough script to configure and update Libreboot image, then flash internally 
#    Copyright (C) 2019 Eliot Roxbergh <e@r0x.se> 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


#!/bin/bash
# !! USAGE: This script is best run MANUALLY
# as it only shows the history of what steps we performed
# to update the Libreboot image. !!

# The last step, to flash internally, requires the setting the kernel parameter iomem=relaxed

WORKDIR="/home/user/libreboot"

#Changed to MAC address in ROM to our.
cd "$WORKDIR"
cp bin/grub/x200_8mb/x200_8mb_usqwerty_vesafb.rom resources/utilities/ich9deblob/x200.rom
cd resources/utilities/ich9deblob/
./ich9gen --macaddress "00:DE:AD:BE:EF:00" #!insert you MAC-address here
dd if=ich9fdgbe_8m.bin of=x200.rom bs=1 count=12K conv=notrunc
mv x200.rom "$WORKDIR" 
cd "$WORKDIR" 

#We customized grub.cfg with custom menu items for booting automatically

#Add a password to GRUB to ensure that unauthorized users can't boot.
#(the password hash is put in grub.cfg)
#In Linux generate a hashed and salted password for GRUB with
grub-mkpasswd-pbkdf2
#COPY THE RESULT, and can then be added in grub.cfg,
#  as follows, grub_password_pbkdf2 user password_options_salt_hash

# To allow anyone to boot operating systems that we have signed
#   (and to e.g. shutdown the computer from GRUB),
# we added the --unrestricted option to these menu options.
#   Other less safe options require admin access in GRUB.

# Created a PGP key and exported its public key
cd "$WORKDIR"
gpg --default-new-key-algo rsa4096 --gen-key
gpg --ouput public.key --export

#Created symlinks
cd /boot
ln -s vmlinuz-4.9.0-9-amd64 vmlinuz
ln -s initrd.img-4.9.0-9-amd64 initrd.img
#Signed the kernel and the initramfs image with our PGP-key
sudo -E gpg --detach-sign vmlinuz-4.9.0-9-amd64
sudo -E gpg --detach-sign initrd.img-4.9.0-9-amd64
ln -s vmlinuz-4.9.0-9-amd64.sig vmlinuz.sig
ln -s initrd.img-4.9.0-9-amd64.sig initrd.img.sig
cd "$WORKDIR"

#We then enabled GRUB to require signatures by adding: set check_signatures=enforce 

#The luks header was extracted and signed with the PGP-key
cryptsetup luksHeaderBackup /dev/sdb2 --header-backup-file luks_header
sudo -E gpg --detach-sign luks_header

#The changes were flashed permanently to the boot flash.
cd "$WORKDIR"
./cbfstool x200.rom extract -n grub.cfg -f grubtest.cfg
./crossgcc/util/cbfstool/cbfstool x200.rom remove -n grubtest.cfg
./crossgcc/util/cbfstool/cbfstool x200.rom add -n grubtest.cfg -f grubtest.cfg -t raw

./crossgcc/util/cbfstool/cbfstool x200.rom remove -n background.png
./crossgcc/util/cbfstool/cbfstool x200.rom add -n background.png -f background.png -t raw

./crossgcc/util/cbfstool/cbfstool x200.rom remove -n grub.cfg
./crossgcc/util/cbfstool/cbfstool x200.rom add -n grub.cfg -f grub.cfg -t raw

./crossgcc/util/cbfstool/cbfstool x200.rom add -n public.key -f public.key -t raw

./crossgcc/util/cbfstool/cbfstool x200.rom add -n luks_header -f luks_header -t raw
./crossgcc/util/cbfstool/cbfstool x200.rom add -n luks_header.sig -f luks_header.sig -t raw
sudo ./flashrom -p internal -w x200.rom #requires kernel parameter iomem=relaxed

