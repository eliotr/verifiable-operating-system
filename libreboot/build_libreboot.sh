#!/bin/bash -e
#Tested 2019-05 with Debian 9.9 AMD64

#    Script to build Libreboot for Debian
#    Copyright (C) 2019 Eliot Roxbergh <e@r0x.se> 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


# --- STEP 0 - Get libreboot and prerequisites ---

cd libreboot
./download flashrom
cd flashrom
sudo apt-get install libusb-1.0-0-dev libpci-dev linux-image-$(uname -r) -y
sudo apt install make gcc pkg-config libssl1.0-dev zlib1g-dev pciutils-dev libftdi-dev \
  libusb-dev build-essential -y
make install #needs sudo? 
make
cd ..
#backup current rom
sudo ./flashrom  -p internal -r ~/this_is_my0.rom


# --- STEP 1 - build libreboot ---

#dependencies for Debian
sudo ./oldbuild dependencies trisquel7

#download and build necessary
./download grub coreboot crossgcc seabios

./oldbuild module crossgcc
./oldbuild module grub
./oldbuild module coreboot
./oldbuild module seabios

cd resources/utilities/ich9deblob
make
cd ../../../

cd ./crossgcc/util/cbfstool/
make
cd ../../../


# --- STEP 2 - build the ROM we want ---

./oldbuild roms withgrub x200_8mb

cp bin/grub/x200_8mb/x200_8mb_usqwerty_vesafb.rom resources/utilities/ich9deblob/x200.rom
cd resources/utilities/ich9deblob/
./ich9gen --macaddress "00:DE:AD:BE:EF:00" #!insert you MAC-address here
dd if=ich9fdgbe_8m.bin of=x200.rom bs=1 count=12K conv=notrunc
mv x200.rom ../../../
cd ../../../

#Optional: add or modify files in the rom
#Finally flash
